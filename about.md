---
layout: page
title: Acerca de
permalink: /about/
---


Recorda que podes **contactar** con nós das seguintes formas:  

+ Twitter: <https://twitter.com/AquiHaiTema>
+ Facebook: <https://fb.me/aquihaitema>
+ Correo: <aquihaitema@gmail.com>
+ Web: <https://aquihaitema.gitlab.io/>
+ Feed Podcast: <https://aquihaitema.gitlab.io/feed>
